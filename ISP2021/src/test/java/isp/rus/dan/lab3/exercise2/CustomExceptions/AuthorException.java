package isp.rus.dan.lab3.exercise2.CustomExceptions;

public class AuthorException extends RuntimeException{
    public AuthorException(String message){ super(message); }
}
