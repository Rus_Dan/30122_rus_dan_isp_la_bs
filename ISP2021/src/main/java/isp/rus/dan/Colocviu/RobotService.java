package isp.rus.dan.Colocviu;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class RobotService {
    private static int LAST_ROBOT_ID = 0;

    public int getLastRobotId(){
        return LAST_ROBOT_ID;
    }

    private List<Robot> robots = new ArrayList<>();

    public void addRobot(Robot robot){
        this.robots.add(robot);
        LAST_ROBOT_ID += 1;
    }

    public void findRobotByIdAndDelete(int id) {
        var foundRobot = this.robots.stream()
                .filter(e -> e.getId() == id)
                .collect(Collectors.toList())
                .get(0);
        if (foundRobot != null) {
            System.out.println(foundRobot);
            this.robots = this.robots.stream()
                    .filter(e -> e.getId() != id)
                    .collect(Collectors.toList());
        }
    }

    public ArrayList<Robot> getRobotByArmsAndSortDescending(int arms){
        // < arms
        var sortedResult = this.robots
                .stream()
                .filter(e -> e.getArms() < arms).sorted().collect(Collectors.toList());
        return (ArrayList<Robot>) sortedResult;
    }

    public void writeIdsThatPassBrandFilter(String brand){
        var ids = this.robots
                .stream()
                .filter(e -> e.getBrand().equals(brand))
                .map(Robot::getId)
                .collect(Collectors.toList());
        WriteToFile.write(ids);
    }
}
