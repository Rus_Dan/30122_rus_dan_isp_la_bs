package isp.rus.dan.Colocviu;

import javax.swing.*;


public class MainView {
    private final RobotService robotService  = new RobotService();
    private JTextArea robotsListView;
    private JPanel mainPanel;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton seeFilteredRobotsButton;
    private JButton addRobotButton;
    private JTextField armsFilterField;

    private void clearFields(){
        armsFilterField.setText("");
        textField1.setText("");
        textField2.setText("");
        textField3.setText("");
    }

    public MainView() {
        seeFilteredRobotsButton.addActionListener(e -> {
            robotsListView.setText("");
            robotService.writeIdsThatPassBrandFilter(armsFilterField.getText());
            var ids = ReadFile.getIds();
            ids.forEach(q -> robotsListView.setText(robotsListView.getText() + q.toString() + "\n"));
            this.clearFields();
        });

        addRobotButton.addActionListener(e -> {
            this.robotService.addRobot(new Robot(this.robotService.getLastRobotId(), textField1.getText(), textField2.getText(), Integer.parseInt(textField3.getText())));
            this.clearFields();
        });
    }

    public static void main(String[] args) {
        var frame = new JFrame("Robots Application");
        frame.setContentPane(new MainView().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
