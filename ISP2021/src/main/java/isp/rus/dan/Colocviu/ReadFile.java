package isp.rus.dan.Colocviu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ReadFile {
    public static List<Integer> getIds() {
        try {
            List<Integer> ids = new ArrayList<>();
            File myObj = new File("robots.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                ids.add(Integer.parseInt(data));
            }
            myReader.close();
            return ids;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("");
        }
    }
}
