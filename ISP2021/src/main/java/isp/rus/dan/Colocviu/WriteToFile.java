package isp.rus.dan.Colocviu;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class WriteToFile {
    public static void write(List<Integer> ids){
        try {
            FileWriter myWriter = new FileWriter("robots.txt");
            ids.forEach(e -> {
                try {
                    myWriter.write(e + "\n");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            });
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
