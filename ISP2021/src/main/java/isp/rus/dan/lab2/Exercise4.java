package isp.rus.dan.lab2;
import java.util.Scanner;

public class Exercise4 {
    public static int[] readVector(int[] vect){
        Scanner in = new Scanner(System.in);
        System.out.println("Give Numbers");
        for (int i = 0; i < vect.length; i++) {
            vect[i]=in.nextInt();
        }
        return vect;
    }
    public static void printMax(int[] vect){
        int max=0;
        for (int i = 0; i < vect.length; i++) {
            if (max<vect[i]){
                max=vect[i];
            }
        }
        System.out.println("Maximus is :"+max);
    }
    public static void main(String[] args) {
        int[] vect = new int[10];
        readVector(vect);
        printMax(vect);
        //System.out.println(vect);
    }
}
