package isp.rus.dan.lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {
    public static void run(int guessNumber){
        Scanner scanner = new Scanner(System.in);
        int lives = 3;
        int tryNumber;
        while (lives > 0){
            System.out.println("Give your number here master >>");
            tryNumber = scanner.nextInt();
            if(tryNumber > guessNumber){
                System.out.println("Wrong number, your number is too high!");
                lives -= 1;
            }else if(tryNumber < guessNumber){
                System.out.println("Wrong number, your number is to low!");
                lives -= 1;
            }else {
                System.out.println("You won!");
                break;
            }
            if(lives == 0){
                System.out.println("You lost!");
            }
        }
    }
    public static void main(String[] args) {
        Random objGenerator = new Random();
        int numberToBeGuessed = objGenerator.nextInt(50);
        System.out.println(numberToBeGuessed);
        run(numberToBeGuessed);
    }
}
