package isp.rus.dan.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Exercise3 {
    private static boolean checkIfPrime(int numberToCheck){
        for(int d = 2; d < numberToCheck / 2 + 1; d ++){
            if(numberToCheck % d == 0){
                return false;
            }
        }
        return true;
    }
    public static List<Integer> getPrimeNumbers(int firstNumber, int secondNumber){
        List<Integer> returnList = new ArrayList<>();
        for(int i = firstNumber + 1; i < secondNumber; i++){
            if (checkIfPrime(i)){
                returnList.add(i);
            }
        }
        return returnList;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Give here the first number >>");
        int firstNumberToRead = scanner.nextInt();
        System.out.println("Give here the second number >>");
        int secondNumberToRead = scanner.nextInt();
        List<Integer> lista = getPrimeNumbers(firstNumberToRead, secondNumberToRead);
        System.out.println("List is >>");
        lista.forEach(System.out::println);
        System.out.println("List size is >>");
        System.out.println(lista.size());
    }
}
