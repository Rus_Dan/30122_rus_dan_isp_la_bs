package isp.rus.dan.lab2;

import java.util.Random;

public class Exercise5 {
    private static final int[] array = new int[10];
    public static void generateArray(int bound) {
        Random objGenerator = new Random();
        for (int iCount = 0; iCount< 10; iCount++){
            int randomNumber = objGenerator.nextInt(bound);
            array[iCount] = randomNumber;
        }
    }
    private static void bubbleSort()
    {
        int n = 10;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (Exercise5.array[j] > Exercise5.array[j+1])
                {
                    // swap arr[j+1] and arr[j]
                    int temp = Exercise5.array[j];
                    Exercise5.array[j] = Exercise5.array[j+1];
                    Exercise5.array[j+1] = temp;
                }
    }
    public static void main(String[] args) {
        generateArray(100);
        bubbleSort();
        for(int i = 0; i < 10; i++){
            System.out.println(array[i]);
        }
    }
}
