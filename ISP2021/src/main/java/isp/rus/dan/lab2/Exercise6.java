package isp.rus.dan.lab2;
import java.util.Scanner;
public class Exercise6 {
    public static int factorialNonRecursive(int number){
        int aux =1;
        for (int i = 1; i <= number; i++) {
            aux=aux*i;
        }
        return aux;
    }
    public  static int reader(){
        Scanner in = new Scanner(System.in);
        System.out.println("Give one number ");
        return in.nextInt();
    }
    public static int factorialRecursive(int number1){
        if (number1<0) return -1;
        if (number1 == 0) return 1;
        else return number1*factorialRecursive(number1-1);
    }
    public static void main(String[] args) {
        //System.out.println("Factorial is :"+factorialNonRecursive(5));
        int number1 = reader();
        System.out.println(("Factorial is :"+factorialRecursive(number1)));
    }
}
