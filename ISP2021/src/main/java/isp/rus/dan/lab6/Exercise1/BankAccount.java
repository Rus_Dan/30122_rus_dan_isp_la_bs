package isp.rus.dan.lab6.Exercise1;

public class BankAccount {
    private String owner;
    private double balance;

    BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;

    }

    public void withdraw(double amount) {
    }

    public void deposit(double amount){
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount b = (BankAccount) obj;
            return balance == b.balance;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return (int) (owner.hashCode() +balance);
    }

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Nelsom",2000);
        BankAccount b2 = new BankAccount("Grigore",2500);

        if(b1.equals(b2))
            System.out.println("Balance "+b1.balance+" and "+b2.balance+ " are equals");
        else
            System.out.println("Balance "+b1.balance+" and "+b2.balance+ " are NOT equals");

        if(b1.owner.equals(b2.owner))
            System.out.println(b1.owner+" and "+b2.owner+" are the same owner");
        else
            System.out.println(b1.owner+" and "+b2.owner+" are different onwers");
    }
}
