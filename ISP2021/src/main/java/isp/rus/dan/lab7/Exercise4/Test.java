package isp.rus.dan.lab7.Exercise4;

import java.io.IOException;

public class Test {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Car c1 = new Car("BMW",6000);
        Car c2 = new Car();
        CarMaker cm1 = new CarMaker();
        cm1.createCar("Mercedes",5500);
        cm1.saveCar(c1,"out.txt");
        cm1.saveCar(c2,"out.txt");
        cm1.readCar("out.txt");

    }
}
