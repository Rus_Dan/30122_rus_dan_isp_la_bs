package isp.rus.dan.lab7.Exercise4;

import java.io.*;

public class CarMaker {
    Car createCar(String name, double price) {
        Car car = new Car(name, price);
        System.out.println(car + " car is created");
        return car;
    }

    void saveCar(Car car, String file) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(car);
            System.out.println("car " + car + " is saved");
            objectOutputStream.close();


        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }

    public Car readCar(String file) throws ClassNotFoundException, IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
        Car car = (Car) inputStream.readObject();
        System.out.println(car + " read from file!");
        return car;
    }
}
