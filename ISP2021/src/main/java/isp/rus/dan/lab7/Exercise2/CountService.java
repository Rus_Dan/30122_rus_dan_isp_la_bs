package isp.rus.dan.lab7.Exercise2;

import javax.imageio.IIOException;
import java.io.*;
import java.util.Scanner;

public class CountService {
    public static void readFile() throws FileNotFoundException {
        String path = "C:\\Laboratoare ISP\\30122_rus_dan_isp_la_bs\\ISP2021\\src\\main\\java\\isp\\rus\\dan\\lab7\\Exercise2\\data.txt";
        String line = "";
        int count = 0;
        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null){
                System.out.println(line);
                for (int i = 0; i < line.length(); i++) {
                    if(line.charAt(i) == 'e'){
                        count++;
                    }
                }
            }
        }catch (FileNotFoundException e){
            System.out.println("File not found");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Number of 'e' appears is: "+count);
    }

}
