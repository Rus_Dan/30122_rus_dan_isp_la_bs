package isp.rus.dan.lab7.Exercise1;

public class CofeeDrinker {

    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, CofeeNrException {
        if (c.getNumber() > 7)
            throw new CofeeNrException(c.getNumber(), "To many coffees: ");
        if (c.getTemp() > 60)
            throw new TemperatureException(c.getTemp(), "Coffee is to hot!");
        if (c.getConc() > 50)
            throw new ConcentrationException(c.getConc(), "Coffee concentration to high!");
        System.out.println("Drink coffee:" + c);
    }
}
