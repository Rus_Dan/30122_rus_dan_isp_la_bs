package isp.rus.dan.lab7.Exercise1;

public class CofeeNrException extends Exception{

    int n;
    public CofeeNrException(int n,String msg){
        super(msg);
        this.n = n;
    }

    int getNumber(){
        return n;
    }
}
