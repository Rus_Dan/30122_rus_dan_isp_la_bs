package isp.rus.dan.lab7.Exercise1;

public class Cofee {
    private int temp;
    private int conc;
    private int number;

    Cofee(int t, int c, int n) {
        temp = t;
        conc = c;
        number = n++;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    int getNumber(){
        return number;
    }

    public String toString() {
        return "[coffee temperature=" + temp + ":concentration=" + conc + ":number" + number +"]";
    }
}
