package isp.rus.dan.lab7.Exercise4;

import java.io.Serializable;

public class Car implements Serializable {

    private String name;
    private double price;

    public Car() {
        this.name = "Dacia";
        this.price = 2000;
    }

    public Car(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
