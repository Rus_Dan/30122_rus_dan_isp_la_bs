package isp.rus.dan.lab7.Exercise3;


import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) throws FileNotFoundException {

        Scanner i = new Scanner(System.in);
        System.out.println("Select an option 1 for encrypt or 2 for decrypt");
        int x = i.nextInt();
        switch (x){
            case 1:
                Encrypt.Encrypt();
                break;
            case 2:
                Decrypt.Decrypt();
                break;
            default:
                System.out.println("Incorrect value for x");
        }

    }
}
