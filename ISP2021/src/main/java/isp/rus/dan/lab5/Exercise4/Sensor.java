package isp.rus.dan.lab5.Exercise4;

public abstract class Sensor {
    private String location;
    public abstract int readValue();
    public String getLocation(){
        return location;
    }
}
