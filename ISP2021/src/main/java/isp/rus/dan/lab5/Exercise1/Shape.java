package isp.rus.dan.lab5.Exercise1;

public abstract class Shape {
    protected String color;
    protected boolean filled=true;

    public Shape() {
        this.color="green";
        this.filled= true;

    }

    public Shape (String color, boolean filled) {
        this.color  = color;
        this.filled = filled;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color) {

        this.color = color;
    }

    public boolean isFilled() {

        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    public abstract double getArea();

    public abstract double getPerimeter();

    @Override
    public String toString(){
        if(this.filled)
            return "A Shape with color of " + this.color + " and filled";
        else
            return "A shape with color of " +this.color + " and not filled";
    }
}
