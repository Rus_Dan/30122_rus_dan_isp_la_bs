package isp.rus.dan.lab5.Exercise4;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    public int readValue(){
        Random random= new Random();
        int val = random.nextInt(100);
        return val;
    }
}

