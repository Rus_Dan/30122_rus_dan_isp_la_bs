package isp.rus.dan.lab5.Exercise2;

public class ProxyImage implements Image {
    private RealImage realImage;
    private String fileName;
    boolean chImage;
    private Image image;

    public ProxyImage(String fileName, boolean chImage){
        this.fileName = fileName;
        this.chImage = chImage;
    }
    @Override
    public void display() {
        if(!chImage) {
            if (image == null) {
                image = new RealImage(fileName);
            }
        }
        else {
            if (image == null) {
                image = new RotatedImage(fileName);
            }
        }
        image.display();
    }

    public static void main(String[] args) {
        ProxyImage I=new ProxyImage("Rus Dan",true);
        I.display();
    }
}
