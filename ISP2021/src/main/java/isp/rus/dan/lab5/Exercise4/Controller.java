package isp.rus.dan.lab5.Exercise4;

public class Controller {
    public void control(){
        TemperatureSensor temp =null;
        LightSensor light = null;
        synchronized (Controller.class) {
            if (temp == null || light == null) {
                temp = new TemperatureSensor();
                light = new LightSensor();
            }
            for (int i = 0; i < 20; i++) {
                System.out.println("Temperature: " + temp.readValue() + " light values: " + light.readValue());
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
