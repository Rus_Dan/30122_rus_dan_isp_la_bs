package isp.rus.dan.lab5.Exercise1;

public class Rectangle extends Shape {
    protected double width;
    protected double length;

    public Rectangle(){
        this.width=1.0;
        this.length=2.0;
    }

    @Override
    public double getArea() {
        return getLength() * getWidth();
    }

    @Override
    public double getPerimeter() {
        return 2*(getWidth()+getLength());
    }

    public Rectangle(double width, double length)
    {
        this.width=width;
        this.length=length;
    }
    public Rectangle(double width, double length, String color, boolean filled){
        super(color,filled);
        this.width = width;
        this.length = length;

    }
    public double getWidth(){
        return width;
    }
    public void setWidth(double width){
        this.width=width;
    }
    public double getLength(){
        return length;
    }
    public void setLength(double length){
        this.length=length;
    }
    public String tostring(){
        return "A Rectangle with width=" + width + "and length=" + length + ",which is a subclass of "+ super.toString() ;
    }
}
