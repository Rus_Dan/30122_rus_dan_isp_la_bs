package isp.rus.dan.lab5.Exercise1;

public class Main {
    public static void main(String[] args) {

        Circle c1 =new Circle(18,"blue",true);
        System.out.println(c1.toString());
        System.out.println("Area:"+c1.getArea()+"Perimeter:" +c1.getPerimeter());

        Rectangle r1= new Rectangle(6,7,"black",true) ;
        System.out.println(r1.toString());
        System.out.println("Area:"+r1.getArea()+"Perimeter:" +r1.getPerimeter());

        Square s1=new Square(12,"green",false);
        System.out.println(s1.toString());
    }
}
