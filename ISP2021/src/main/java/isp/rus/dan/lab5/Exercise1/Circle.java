package isp.rus.dan.lab5.Exercise1;

public class Circle extends Shape {
    protected double radius;

    public Circle() {
        this.radius = 1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle (double radius, String color, boolean filled) {
        this.color=color;
        this.radius=radius;
    }


    public double getRadius() {
        return radius;
    }


    public void setRadius(double radius) {
        this.radius = radius;
    }


    public double getArea() {
        return 3.14 * Math.pow(getRadius(),2);
    }
    public double getPerimeter() {
        return 2 * 3.14 * getRadius();
    }
    public String toString(){
        return "A Circle with radius=" + radius + " which is a subclass of "+ super.toString() ;
    }
}
