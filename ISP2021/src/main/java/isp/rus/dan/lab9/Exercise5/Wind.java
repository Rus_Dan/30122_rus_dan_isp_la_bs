package isp.rus.dan.lab9.Exercise5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Wind extends JFrame implements ActionListener {
    private JTextField tF1,tF2,tF3,t1,t2;
    private JButton b1;
    Segment s;
    Wind(Controler co1,Controler co2, Controler co3,int n,Segment s){
        setTitle("Train");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(1280,800);
        tF1 = new JTextField();
        tF1.setBounds(30,20,1000,100);
        tF1.setText(co1.stationState());
        add(tF1);
        tF2 = new JTextField();
        tF2.setBounds(30,130,1000,100);
        tF2.setText(co2.stationState());
        add(tF2);
        tF3 = new JTextField();
        tF3.setBounds(30,240,1000,100);
        tF3.setText(co3.stationState());
        add(tF3);
        t1 = new JTextField();
        t1.setBounds(30,600,300,20);
        t2 = new JTextField();
        t2.setBounds(30,640,300,20);
        b1 = new JButton();
        b1.setBounds(30,670,100,20);
        b1.setText("New train");
        add(t1);add(t2);add(b1);
        this.s=s;
        b1.addActionListener(this);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(b1)){
            Train t = new Train(t1.getText(),t2.getText());
            s.arriveTrain(t);
        }
    }

}
