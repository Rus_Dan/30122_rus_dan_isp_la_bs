package isp.rus.dan.lab9.Exercise2;

import java.awt.*;
import java.awt.event.*;

public class Counter extends Frame{
    private Label counterLabel;
    private TextField countText;
    private Button button;
    private int count = 0;

    public Counter() {
        setLayout(new FlowLayout());
        counterLabel = new Label("Counter");
        add(counterLabel);

        countText = new TextField(count + "", 10);
        countText.setEditable(false);
        add(countText);
        button = new Button("Count");
        add(button);
        BtnCountListener listener = new BtnCountListener();
        button.addActionListener(listener);
        setTitle("Button Push Counter");  //
        setSize(300, 200);
        setVisible(true);
    }


    public static void main(String[] args) {
        Counter buttonPushCounter = new Counter();
    }

    private class BtnCountListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent evt) {
            ++count;
            countText.setText(count + "");
        }
    }
}
