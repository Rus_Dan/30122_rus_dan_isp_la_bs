package isp.rus.dan.lab9.Exercise3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class FileReader extends Frame {
    JTextArea textReaded;
    private Button readFileButton;
    private Label fileName;
    private TextField text;
    String a = "A";

    public FileReader() {
        setLayout(new FlowLayout());
        fileName = new Label("enter the file name:");
        add(fileName);

        text = new TextField(a,10);
        text.setEditable(true);
        add(text);

        textReaded = new JTextArea(5,10);
        add(textReaded);

        readFileButton = new Button("read");
        add(readFileButton);
        ReadFileButtonListener listener = new ReadFileButtonListener();
        readFileButton.addActionListener(listener);

        setTitle("Read file");
        setSize(500,500);
        setVisible(true);
    }

    void open(String enteredFileName){
        try {
            textReaded.setText(" ");
            BufferedReader bf =
                    new BufferedReader(
                            new java.io.FileReader(new File(enteredFileName)));
            String l = " ";
            l = bf.readLine();
            while (l != null) {
                textReaded.append(l + "\n");
                l = bf.readLine();
            }
        }catch (Exception e){}
    }


    public static void main(String[] args) {
        FileReader ex4 = new FileReader();
    }

    private class ReadFileButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent evt) {
            File dir = new File("E:/Egyetem/SEM II/ISP/Lab/30122_zsolt_kulcsar_isp_labs/ISP2021/src/main/java/isp/kulcsarzsolt/Lab9/Exercise3");
            open(dir + a);
            readFileButton.setBackground(Color.BLUE);
        }
    }

}
