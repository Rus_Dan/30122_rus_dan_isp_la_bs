package isp.rus.dan.lab10.Exercise4;

public class Point {
    private int x;
    private int y;
    private Robot robot;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void departRobot(Robot robot) {
        this.robot = robot;
    }

    public Robot getRobot() {
        return robot;
    }

    public void deleteRobot() {
        robot = null;
    }
}
