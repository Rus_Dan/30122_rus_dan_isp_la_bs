package isp.rus.dan.lab10.Exercise4;

public class Simulation {
    public static void main(String[] args) {
        final int X_LENGTH = 100;
        final int Y_LENGTH = 100;

        Map robotMap = new Map(X_LENGTH,Y_LENGTH);

        // Declare Robots
        Robot Robot1 = new Robot("Daniela",new Point(0,0),robotMap);
        Robot Robot2 = new Robot("Tudor",new Point(10,10),robotMap);
        Robot Robot3 = new Robot("Lavinia",new Point(20,20),robotMap);
        Robot Robot4 = new Robot("Alexandru",new Point(30,30),robotMap);
        Robot Robot5 = new Robot("Ioana",new Point(50,50),robotMap);
        Robot Robot6 = new Robot("Matei",new Point(40,40),robotMap);
        Robot Robot7 = new Robot("Alina",new Point(60,60),robotMap);
        Robot Robot8 = new Robot("Gloria",new Point(70,70),robotMap);
        Robot Robot9 = new Robot("Criss",new Point(80,80),robotMap);
        Robot Robot10 = new Robot("Ianis",new Point(90,90),robotMap);

        // Start Multiple Threads
        Robot1.start();
        Robot2.start();
        Robot3.start();
        Robot4.start();
        Robot5.start();
        Robot6.start();
        Robot7.start();
        Robot8.start();
        Robot9.start();
        Robot10.start();
    }
}
