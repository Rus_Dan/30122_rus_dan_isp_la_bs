package isp.rus.dan.lab10.Exercise6;

public class Chronometer {
    private int minutes;
    private int seconds;
    private int miliseconds;
    private boolean stopwatchStatus;

    public Chronometer() {
        this.miliseconds = 0;
        this.seconds = 0;
        this.minutes = 0;
        stopwatchStatus = false;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    public int getMiliseconds() {
        return miliseconds;
    }

    public void setMiliseconds(int miliseconds) {
        this.miliseconds = miliseconds;
    }

    public void setStopwatchStatus(boolean stopwatchStatus) {
        this.stopwatchStatus = stopwatchStatus;
    }

    synchronized void reset() {
        this.miliseconds = 0;
        this.seconds = 0;
        this.minutes = 0;
    }

    public void transferData() throws InterruptedException {
        synchronized (this){
            GraphicChronometer.chronometer.setText("" + minutes / 10 + minutes % 10 + ":" +
                    seconds / 10 + seconds % 10 + ":" +
                    miliseconds / 10 + miliseconds % 10);
            if(stopwatchStatus){
                notify();
            }
        }

    }

    public void incrementTime() throws InterruptedException {
        synchronized (this) {
            Thread.sleep(10);
            if(stopwatchStatus){
                miliseconds++;
            }
            try {
                if (miliseconds >= 100) {
                    miliseconds = 0;
                    seconds++;
                }
                if (seconds >= 60) {
                    seconds = 0;
                    minutes++;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            wait();
        }
    }
}
