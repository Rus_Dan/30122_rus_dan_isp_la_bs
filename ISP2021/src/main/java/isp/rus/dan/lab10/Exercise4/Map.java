package isp.rus.dan.lab10.Exercise4;

public class Map {
    private final Point[][] twoDMap;

    public Map(int xLength, int yLength) {
        twoDMap = new Point[yLength][xLength];

        for (int y = 0; y < xLength; y++) {
            for (int x = 0; x < yLength; x++) {
                twoDMap[y][x] = new Point(y, x);
            }
        }
    }

    public Point[][] getTwoDMap() {
        return twoDMap;
    }
}
