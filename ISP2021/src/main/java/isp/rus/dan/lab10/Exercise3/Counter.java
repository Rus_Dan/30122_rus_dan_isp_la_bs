package isp.rus.dan.lab10.Exercise3;

public class Counter extends Thread {
    int startPoint = 0;
    int endPoint = 0;
    Thread t;

    public Counter(String name, int startPoint,int endPoint, Thread t) {
        super(name);
        this.t = t;
        this.startPoint=startPoint;
        this.endPoint=endPoint;
    }

    public void run() {
        try {
            if (t != null) {
                t.join();
            }
            for (int i = this.startPoint; i < this.endPoint; i++) {
                System.out.println(getName() + " i = " + i);
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println(getName() + " job finalised.");
        }
    }
}
