package isp.rus.dan.lab10.Exercise6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class GraphicChronometer extends JFrame {
    private final int WIDTH = 100;
    private final int HEIGHT = 20;

    private Thread t1;
    private Thread t2;

    private final Chronometer myChronometer = new Chronometer();

    private JButton startStopButton;
    private JButton resetButton;

    public static JTextField chronometer;

    public GraphicChronometer() {
        setTitle("Chronometer by Rus Dan");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(new Dimension(640, 480));
        setLayout(null);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - getSize().width / 2, dim.height / 2 - getSize().height / 2);
        init();
        setVisible(true);
    }

    private void init() {
        t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        myChronometer.incrementTime();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        myChronometer.transferData();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        chronometer = new JTextField("00:00:00");
        chronometer.setBounds(250, 100, (int) (WIDTH * 1.25), HEIGHT * 2);
        chronometer.setHorizontalAlignment(JTextField.CENTER);
        chronometer.setEditable(false);
        chronometer.setFont(new Font("Courier", Font.BOLD, 22));

        startStopButton = new JButton("Start");
        startStopButton.setBounds(chronometer.getX() + 60, chronometer.getY() * 2, WIDTH, HEIGHT * 2);
        startStopButton.setFocusPainted(false);
        startStopButton.addActionListener(new StartStop());

        resetButton = new JButton("Reset");
        resetButton.setBounds(startStopButton.getX() - 100, startStopButton.getY(), WIDTH, HEIGHT * 2);
        resetButton.setFocusPainted(false);
        resetButton.addActionListener(new Reset());

        t1.start();
        t2.start();

        this.add(chronometer);
        this.add(startStopButton);
        this.add(resetButton);
        repaint();
    }

    private class StartStop implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (((JButton) e.getSource()).getText().equals("Start")) {
                ((JButton) e.getSource()).setText("Pause");
                myChronometer.setStopwatchStatus(true);
            } else {
                ((JButton) e.getSource()).setText("Start");
                myChronometer.setStopwatchStatus(false);

            }
        }
    }


    private class Reset implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (((JButton) e.getSource()).getText().equals("Reset")) {
                myChronometer.setStopwatchStatus(false);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                myChronometer.setMiliseconds(0);
                myChronometer.setMinutes(0);
                myChronometer.setSeconds(0);
                startStopButton.setText("Start");
            }
        }
    }
}
