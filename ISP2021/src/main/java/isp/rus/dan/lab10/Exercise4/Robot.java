package isp.rus.dan.lab10.Exercise4;

import java.util.Random;

public class Robot extends Thread {
    private final String robotName;
    private final Point position;
    private final Map robotMap;

    private final Random rand = new Random();
    private boolean canStart = true;

    public Robot(String robotName, Point position, Map robotMap) {
        this.robotName = robotName;
        this.position = position;
        this.robotMap = robotMap;
        verifyPuttingError();
    }

    public void run() {
        if (!canStart) {
            this.stop();
        }
        int newX;
        int newY;
        for (; ; ) {
            Point newAdderPosition = getNewAdderPosition();
            Robot checkNextPosition = getRobotFromMap(newAdderPosition);

            // Check Collide Conditions
            if (checkNextPosition != null &&
                    ((newAdderPosition.getX() != 0) || newAdderPosition.getY() != 0)) {
                newX = position.getX() + newAdderPosition.getX();
                newY = position.getY() + newAdderPosition.getY();

                System.out.println(robotName + " (" + position.getX() + "," + position.getY() + ") -> " +
                        newX + " " + newY + ". " + robotName + " crashed with " + checkNextPosition.getRobotName() +
                        ". The crashed position is: (" + checkNextPosition.position.getX() + ","
                        + checkNextPosition.position.getY() + ")");
                // Delete next position Robot
                checkNextPosition.robotMap.getTwoDMap()
                        [checkNextPosition.position.getY()]
                        [checkNextPosition.position.getX()]
                        .deleteRobot();
                checkNextPosition.stop();

                // Delete current Robot
                robotMap.getTwoDMap()
                        [this.position.getY()]
                        [this.position.getX()]
                        .deleteRobot();
                this.stop();
            } else {
                // delete from the last position
                this.robotMap.getTwoDMap()
                        [position.getY()]
                        [position.getX()]
                        .deleteRobot();
                newX = position.getX() + newAdderPosition.getX();
                newY = position.getY() + newAdderPosition.getY();

                System.out.println(robotName + " (" + position.getX() + "," + position.getY() + ") -> " +
                        "(" + newX + "," + newY + ")");
                //put in the next position
                this.position.setXY(position.getX() + newAdderPosition.getX(), position.getY() + newAdderPosition.getY());
                this.robotMap.getTwoDMap()
                        [position.getY()]
                        [position.getX()]
                        .departRobot(this);

                // repaus time
                try {
                    int MAXIMUM_REPAUS_TIME = 100;
                    Thread.sleep(rand.nextInt(MAXIMUM_REPAUS_TIME));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private Point getNewAdderPosition() {
        int addX = rand.nextInt(3) - 1;
        int addY = rand.nextInt(3) - 1;

        while (addX + position.getX() >= robotMap.getTwoDMap()[0].length ||
                addX + position.getX() < 0) {
            addX = rand.nextInt(3) - 1;
        }
        while (addY + position.getY() >= robotMap.getTwoDMap().length ||
                addY + position.getY() < 0) {
            addY = rand.nextInt(3) - 1;
        }
        return new Point(addX, addY);
    }

    private Robot getRobotFromMap(Point adderPoint) {
        return robotMap.getTwoDMap()
                [adderPoint.getY() + position.getY()]
                [adderPoint.getX() + position.getX()]
                .getRobot();
    }

    public String getRobotName() {
        return robotName;
    }

    public void verifyPuttingError() {
        Robot getRobotFromCurrentPosition = getRobotFromMap(new Point(0, 0));
        if (getRobotFromCurrentPosition == null) {
            this.robotMap.getTwoDMap()[position.getY()][position.getX()].departRobot(this);
            System.out.println(robotName + " placed at : " + position.getX() + "," + position.getY());
        } else {
            System.out.println(robotName + " crashed when placed with " + getRobotFromCurrentPosition.getRobotName());
            robotMap.getTwoDMap()[position.getY()][position.getX()].deleteRobot();
            this.canStart = false;
            getRobotFromCurrentPosition.canStart = false;
        }
    }
}
