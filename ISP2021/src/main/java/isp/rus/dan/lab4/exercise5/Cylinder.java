package isp.rus.dan.lab4.exercise5;

public class Cylinder extends Circle {
    private double height;
    Cylinder ()
    {
        this.height=1.0;
    }
    Cylinder (double radius){
        super(radius);
    }
    Cylinder (double height,double radius)
    {   super(radius);
        this.height=height;
    }
    public double getHeight()
    {
        return height;
    }
    public double getArea(){
        return 2*3.14*super.getRadius()*this.height+2*super.getArea();
    }
    public double getVolume()
    {
        return super.getArea()*height;
    }
}
