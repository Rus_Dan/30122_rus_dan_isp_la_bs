package isp.rus.dan.lab4.exercise6;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rectangle=new Rectangle(2.0, 3.0, "Blue", true);
        System.out.println(rectangle.toString());
        System.out.println(rectangle.getArea());
        Square square=new Square(2.0,"red",false);
        System.out.println(square.toString());
        Circle circle=new Circle(1.0 ,"Yellow",true);
        System.out.println(circle.toString());
    }
}
