package isp.rus.dan.lab4.exercise5;

public class Circle {
    private double radius;
    private String color;

    Circle(){
        radius=1.0;
        color="red";
    }
    Circle(double radius)
    {
        this.radius=radius;
    }
    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return 3.14*radius*radius;
    }
}
