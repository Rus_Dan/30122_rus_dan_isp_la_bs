package isp.rus.dan.lab4.exercise4;

public class Book {
    private String name;
    private Author[] Authors;
    private double price;
    private int qtyInStock = 0;

    Book(String name, Author[] Authors, double price) {
        this.name = name;
        this.Authors = Authors;
        this.price = price;
    }

    Book(String name, Author[] Authors, double price, int qtyInStock) {
        this.name = name;
        this.Authors = Authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {

        return name;
    }

    public Author[] getAuthors() {
        return Authors;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {

        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void printAuthors() {
        for (int i = 0; i < Authors.length; i++) {
            System.out.println(Authors[i].toString()+" ");
        }
    }
    public String toString(){
        return "Book "+getName()+" written by "+Authors.length+" authors";
    }
}
