package isp.rus.dan.lab4.exercise5;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder cylinder=new Cylinder(2.0 ,2.0);
        System.out.println("The volume of the cylinder is : "+cylinder.getVolume());
        System.out.println("The area of the cylinder is : "+cylinder.getArea());
    }
}
