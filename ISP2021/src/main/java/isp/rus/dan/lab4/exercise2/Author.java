package isp.rus.dan.lab4.exercise2;

import isp.rus.dan.lab3.exercise3.AuthorGenderException;

public class Author {
    private final String name;
    private String email;
    private final char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        if(gender == 'm' || gender == 'f')
            this.gender = gender;
        else
            throw new AuthorGenderException("The gender must be either f (female)/ m(male) !!");
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return this.name +" (" + this.gender + ") "+ " at " + this.email;
    }
}
