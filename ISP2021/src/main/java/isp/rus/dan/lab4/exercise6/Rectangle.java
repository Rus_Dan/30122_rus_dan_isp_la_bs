package isp.rus.dan.lab4.exercise6;

public class Rectangle extends Shape {
    private double width;
    private double length;

    Rectangle() {
        width = 1.0;
        length = 1.0;
    }

    Rectangle(double width, double length) {
        this.length = length;
        this.width = width;
    }

    Rectangle(double width, double length, String color, boolean filled) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return width * length;
    }

    public double getPerimeter() {
        return 2 * width + 2 * length;
    }

    public String toString() {
        return "A rectangle with the length of " + length + " centimeters, and the width of " + width + " centimeters, which is a subclass of " + super.toString();
    }

}
