package isp.rus.dan.lab4.exercise6;

public class Shape {
    private String color;
    private boolean filled;

    Shape() {
        color = "green";
        filled = true;
    }

    Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        if (isFilled() == true) return "A Shape colored " + color + " and is filled ";
        else return "A shape colored " + color + "and is not filled";
    }
}
