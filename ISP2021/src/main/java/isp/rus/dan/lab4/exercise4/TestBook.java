package isp.rus.dan.lab4.exercise4;

public class TestBook {

    public static void main(String[] args) {
        Author[] authors=new Author[3];
        authors[0]=new Author("Nicu Paleru","Npaleru@gmail.com",'m');
        authors[1]=new Author("Adelin Petrisor","Apetrisor@Gamil.com",'f');
        authors[2]=new Author("Ionel Cotorobai","Icotorobai@yahoomail.com",'m');
        Book book=new Book("Papillon", authors,50 );
        book.printAuthors();
        System.out.println(book.toString());

    }
}
