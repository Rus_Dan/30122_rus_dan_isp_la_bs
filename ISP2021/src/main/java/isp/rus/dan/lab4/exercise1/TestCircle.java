package isp.rus.dan.lab4.exercise1;

public class TestCircle {
    public static void main(String[] args) {
        Circle circle=new Circle(2.0 );
        System.out.println("A circle with the radius of "+circle.getRadius()+" and an area of "+circle.getArea());
    }
}
