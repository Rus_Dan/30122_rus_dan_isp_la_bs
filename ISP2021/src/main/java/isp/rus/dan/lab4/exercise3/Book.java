package isp.rus.dan.lab4.exercise3;

public class Book  {
    private String name;
    private Author Author;
    private double price;
    private int qtyInStock = 0;

    Book(String name, Author Author, double price) {
        this.name = name;
        this.Author = Author;
        this.price = price;
    }

    Book(String name, Author Author, double price, int qtyInStock) {
        this.name = name;
        this.Author = Author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {

        return name;
    }

    public Author getAuthor() {
        return Author;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQtyInStock(double qtyInStock) {
        this.price = price;
    }
    public String toString(){
        return "Book "+getName()+" written by "+Author.toString();

    }
}
