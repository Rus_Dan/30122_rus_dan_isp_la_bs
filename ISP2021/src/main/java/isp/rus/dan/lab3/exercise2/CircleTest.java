package isp.rus.dan.lab3.exercise2;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    Circle circle = new Circle();
    Circle circleTwo = new Circle(2);
    Circle circleThree = new Circle(3, "blue");

    @org.junit.jupiter.api.Test
    void getRadius() {
        assertEquals(circleTwo.getRadius(), 2);
        assertEquals(circle.getRadius(), 1);
        assertEquals(circleThree.getRadius(), 3);
    }

    @org.junit.jupiter.api.Test
    void getArea() {
        assertEquals(circle.getArea(), circle.getRadius() * circle.getRadius() * Math.PI);
        assertEquals(circleTwo.getArea(), circleTwo.getRadius() * circleTwo.getRadius() * Math.PI);
        assertEquals(circleThree.getArea(), circleThree.getRadius() * circleThree.getRadius() * Math.PI);
    }
}