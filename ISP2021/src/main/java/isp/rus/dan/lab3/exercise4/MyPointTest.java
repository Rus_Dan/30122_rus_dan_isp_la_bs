package isp.rus.dan.lab3.exercise4;

import isp.rus.dan.lab3.exercise4.MyPoint;

import static org.junit.jupiter.api.Assertions.*;

class MyPointTest {

    private final int TEST_X = 2;
    private final int TEST_Y = 2;
    private final int ZERO_VALUE = 0;
    private final double RESULT = 2;
    MyPoint myPointTest = new MyPoint();
    MyPoint myPointTestTwo = new MyPoint(TEST_X, TEST_Y);

    @org.junit.jupiter.api.Test
    void getX() {
        assertEquals(myPointTest.getX(), ZERO_VALUE);
        assertEquals(myPointTestTwo.getX(), TEST_X);
    }

    @org.junit.jupiter.api.Test
    void getY() {
        assertEquals(myPointTest.getY(), ZERO_VALUE);
        assertEquals(myPointTestTwo.getY(), TEST_Y);
    }

    @org.junit.jupiter.api.Test
    void setX() {
        int TEST_X_SETTER = 4;
        myPointTestTwo.setX(TEST_X_SETTER);
        assertEquals(myPointTestTwo.getX(), TEST_X_SETTER);
        myPointTestTwo.setX(TEST_X);
    }

    @org.junit.jupiter.api.Test
    void setY() {
        int TEST_Y_SETTER = 6;
        myPointTestTwo.setY(TEST_Y_SETTER);
        assertEquals(myPointTestTwo.getY(), TEST_Y_SETTER);
        myPointTestTwo.setY(TEST_Y);
    }

    @org.junit.jupiter.api.Test
    void setXY() {
        myPointTestTwo.setXY();
        assertEquals(myPointTestTwo.getX(), ZERO_VALUE);
        myPointTestTwo = new MyPoint(TEST_X, TEST_Y);
    }

    @org.junit.jupiter.api.Test
    void distance() {
        assertEquals(myPointTest.distance(TEST_X, ZERO_VALUE), RESULT);
    }

    @org.junit.jupiter.api.Test
    void testDistance() {
        assertEquals(myPointTest.distance(new MyPoint(ZERO_VALUE, TEST_Y)), RESULT);
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        String result = "(0, 0)";
        assertEquals(myPointTest.toString(), result);
    }
}