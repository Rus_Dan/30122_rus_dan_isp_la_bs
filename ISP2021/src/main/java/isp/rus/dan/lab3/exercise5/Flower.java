package isp.rus.dan.lab3.exercise5;

public class Flower{
    int petal;
    private static int numberOfInstances = 0;
    Flower(){
        numberOfInstances += 1;
        System.out.println("Flower has been created!");
    }

    public static int getNumberOfInstances(){
        return numberOfInstances;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println(Flower.getNumberOfInstances());
    }
}