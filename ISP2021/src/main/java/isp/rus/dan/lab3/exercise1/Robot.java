package isp.rus.dan.lab3.exercise1;

public class Robot {
    int x;

    Robot() {
        x = 1;
    }

    public void change(int k) {
        if (k >= 1)
            x = x + k;
    }

    public String toString() {
        return "Pozition of the robot is "+x;
    }
}
