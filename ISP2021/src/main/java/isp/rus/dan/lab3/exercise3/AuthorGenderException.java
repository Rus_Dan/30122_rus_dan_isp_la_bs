package isp.rus.dan.lab3.exercise3;

public class AuthorGenderException extends RuntimeException{
    public AuthorGenderException(String message) {
        super(message);
    }
}
