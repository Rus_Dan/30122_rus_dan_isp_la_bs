package isp.rus.dan.lab3.exercise2;

public class Circle {
    private final double radius;
    private String color;

    public Circle(){
        this.color = "red";
        this.radius = 1.0;
    }

    public Circle(double radius){
        this.radius = radius;
    }

    public Circle(double radius, String color){
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea(){
        return radius * radius * Math.PI;
    }

}
