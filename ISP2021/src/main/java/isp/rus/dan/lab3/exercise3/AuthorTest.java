package isp.rus.dan.lab3.exercise3;


import static org.junit.jupiter.api.Assertions.*;

class AuthorTest {
    private final String TEST_NAME = "Dori";
    private final String TEST_EMAIL = "dory7141@gmail.com";
    private final char TEST_GENDER = 'm';

    Author testAuthor = new Author(TEST_NAME, TEST_EMAIL, TEST_GENDER);

    @org.junit.jupiter.api.Test
    void testConstructor(){
        char WRONG_GENDER = 'd';
        assertThrows(AuthorGenderException.class ,() -> new Author(TEST_NAME, TEST_EMAIL, WRONG_GENDER));
    }

    @org.junit.jupiter.api.Test
    void getGender() {
        assertEquals(testAuthor.getName(), TEST_NAME);
    }

    @org.junit.jupiter.api.Test
    void getEmail() {
        assertEquals(testAuthor.getEmail(), TEST_EMAIL);
    }

    @org.junit.jupiter.api.Test
    void getName() {
        assertEquals(testAuthor.getGender(), TEST_GENDER);
    }

    @org.junit.jupiter.api.Test
    void setEmail() {
        String SETTER_EMAIL_TEST = "daie2710@cs.cluj.ro";
        testAuthor.setEmail(SETTER_EMAIL_TEST);
        assertEquals(testAuthor.getEmail(), SETTER_EMAIL_TEST);
        testAuthor.setEmail(TEST_EMAIL);
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        String stringBuilder = TEST_NAME +
                " (" +
                TEST_GENDER +
                ") " +
                " at " +
                TEST_EMAIL;
        assertEquals(testAuthor.toString(), stringBuilder);
    }
}