package isp.rus.dan.lab11.Exercise1;

import javax.swing.*;
import java.awt.*;

public class TemperatureApp extends JFrame {
    TemperatureApp(TemperatureTextView tview, TemperatureCanvasView tcanvasView){
        setLayout(new BorderLayout());
        tcanvasView.setPreferredSize(new Dimension(250,250));
        add(tview,BorderLayout.NORTH);
        add(tcanvasView,BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        Thermometer t = new Thermometer();
        t.start();

        TemperatureCanvasView tcanvasView = new TemperatureCanvasView();
        TemperatureTextView tview = new TemperatureTextView();
        TemperatureController tcontroler = new TemperatureController(t,tview,tcanvasView);

        new TemperatureApp(tview,tcanvasView);
    }
}
