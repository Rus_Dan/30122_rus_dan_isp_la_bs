package isp.rus.dan.lab11.Exercise2;

public class Product {
    private String name;
    private int quantity;
    private int price;

    public Product(){
        this.name = null;
        this.quantity = 0;
        this.price = 0;
    }
    public Product(String name, int quantity, int price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return this.name+", "+
                "quantity: "+this.quantity+
                ", price:"+this.price;
    }

    public boolean equals(String product) {
        return this.name.equals(product);
    }
}
