package isp.rus.dan.lab11.Exercise1;

import javax.swing.*;
import java.util.Observer;
import java.awt.*;
import java.util.Observable;

public class TemperatureTextView extends JPanel implements Observer {
    JTextField jtfTemp;
    JLabel jtlTemp;
    JLabel jtlTemp2;
    JButton action;

    TemperatureTextView(){
        this.setLayout(new FlowLayout());
        jtfTemp = new JTextField(20);
        jtlTemp = new JLabel("Temperature");
        action = new JButton("Enable-Disable");
        add(action);add(jtlTemp);add(jtfTemp);
    }

    public void update(Observable o, Object arg) {
        String s = ""+((Thermometer)o).getTemperature();
        jtlTemp2 = new JLabel("Temperature is changed");
        add(jtlTemp2);
        try {Thread.sleep(1000);} catch (InterruptedException e) {}
        remove(jtlTemp2);
        jtfTemp.setText(s);
    }

    public void addEnableDisableListener(TemperatureController.EnableDisableListener listener) {
        action.addActionListener(listener);
    }

}
